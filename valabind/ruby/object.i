%module object
%{
	extern "C" {
	#include <object.h>
	}
	#include <vector>
%}
typedef struct ValaObjectValaClass {
	char* name;
} ValaClass;


%extend ValaClass {
	~ValaClass() {
		g_object_unref(self);
	}
	ValaClass () {
		return vala_object_valaclass_new ();
	}
	const char* append_to_name(char* suffix) {
		return vala_object_valaclass_append_to_name(self, suffix);
	}
};

