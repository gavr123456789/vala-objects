# GObject for Lua

Lua uses GObjects via [lgi](https://github.com/pavouk/lgi).
Kudos to [Pavel Holejsovsky](https://github.com/pavouk).

    sudo apt-get install lua5.1 luarocks
    luarocks install lgi --local
    ./run.sh test.lua

